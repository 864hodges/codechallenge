///
/// The ContactDetailView class is here to maninpulate the view and retrieve data from the user.
///
/// Auther: Mike Hodges
/// Created: 9/18/2015
///
var ContactDetailView = function() {
	
	///
	/// This section manipulates the DOM.
	///
	var UI = {
		///
		/// This method shows the user that the system is busy.
		///
		showWaiting: function(self) {
			var waiting = '<h1></h1>';
			waiting += '<span class="company"></span>';
			waiting += '<h2></h2>';			
			waiting += '<div class="address"></div><br/>';
			waiting += '<img src="images/wait.gif" />';
			waiting += '<h2></h2>';
			waiting += '<div class="address"></div>';
			waiting += '<div class="address"></div>';
			waiting += '<div class="address"></div><br/>';
			waiting += '<div class="url"></div><br/>';
			waiting += '<div class="cb"></div>';
			waiting = $('<div/>').html(waiting);			
			self.contactDisplayArea.html(waiting.html());
		},
		
		///
		/// This method shows the user that the system has deleted a contact.
		///
		showDeleted: function(self, message) {
			if (message) {
				$('.deleted').html(message).show();
			} else {
				$('.deleted').show();
			}	
		},
		///
		/// This method removes the deletion notice.
		///
		hideDeleted: function(self, callback) {
			$('.deleted').hide( "drop", { direction: "down" }, 4000, function() { callback(); });
		},
		
		///
		/// This method shows the user details about the selected contact.
		///
		showContact: function(self, contact) {
			var item = '<h1 prop-id="' + contact.id + '">' + contact.first_name + ' ' + contact.last_name + '</h1>';
			item += '<span class="company">' + contact.company_name + '</span>';
			item += '<div class="line">&nbsp;</div>';
			
			item += '<h2>Address:</h2>';			
			if (contact.address.length === 0 || contact.city.length === 0 || contact.state.length === 0 || contact.zip.length === 0) {
				item += '<div class="address">Address not provided</div><br/>';
			} else {
				item += '<div class="address">' + contact.address + '</div>';
				item += '<div class="address">' + contact.city + '</div>';
				
				if (contact.state && contact.zip) {
					item += '<div class="address">' + contact.state + ', ' + contact.zip + '</div><br/>';
				} else if (contact.state) {
					item += '<div class="address">' + contact.state + '</div><br/>';
				} else {
					item += '<div class="address">' + contact.zip + '</div><br/>';
				}
			}
			
			item += '<h2>Contact Methods:</h2>';
			item += '<div class="address">Phone: ' + (contact.phone || 'Not provided') + '</div>';
			item += '<div class="address">Work Phone: ' + (contact.work_phone || 'Not provided') + '</div>';
			item += '<div class="address"><a href="mailto:' + contact.email + '">' + contact.email + '</a></div><br/>';
			item += '<div class="url"><a href="' + contact.url + '" target="_blank">' + contact.url + '</a></div><br/>';
			item += '<div class="cb"></div>';
			item = $('<div/>').html(item);			
			self.contactDisplayArea.html(item.html());
		}
	};
		
	return {
		eventContext: null,
		contactDisplayArea: null,
		
		///
		/// This method initializes the view instance by accepting refences to its dependencies
		/// and listening for pertinent events.
		///
		init: function(eventContext, displayArea) {
			var self = this;
			this.eventContext = eventContext;
			this.contactDisplayArea = displayArea;
			
			// Listen for user interaction events
			var actionContext = $('#selected').find('#detailMenu');
			$('#editContact', actionContext).bind('click', function() {
				var event = $.Event('editContactClicked', {id:$('h1:first', self.contactDisplayArea).attr('prop-id')});
				self.eventContext.trigger(event);
			});
			$('#deleteContact', actionContext).bind('click', function() {
				var event = $.Event('deleteContactClicked', {id:$('h1:first', self.contactDisplayArea).attr('prop-id')});
				self.eventContext.trigger(event);
			});
			$('#logo').bind('click', function() {
				window.location = "index.html";
			});
			
			// Listen for events that provide data
			self.eventContext.bind('contactSelectedForDisplay', function(contact) {
				UI.showContact(self, contact);
			});
		},
		
		///
		/// This method shows the user that the system is busy.
		///
		showWaiting: function() {
			UI.showWaiting(this);
		},
		///
		/// This method shows the user that the system has deleted a contact.
		///
		showDeleted: function(message) {
			UI.showDeleted(this, message);
		},
		///
		/// This method removes the deletion notice.
		///
		hideDeleted: function(callback) {
			UI.hideDeleted(this, callback);
		},
		
		///
		/// This method shows the user details about the selected contact.
		///
		showContact: function(contact) {
			UI.showContact(this, contact);
		}
	}
};