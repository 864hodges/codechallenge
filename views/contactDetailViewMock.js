var ContactDetailViewMock = function() {
	
	var UI = {
		
		showWaiting: function(self) {
		},
		showDeleted: function(self, message) {
		},
		hideDeleted: function(self, callback) {
		},
		
		showContact: function(self, contact) {
		}
		
	};
		
	return {
		
		eventContext: null,
		
		init: function(eventContext) {
			var self = this;
			this.eventContext = eventContext;
		},
		
		showWaiting: function() {
			UI.showWaiting(this);
		},
		showDeleted: function(message) {
			UI.showDeleted(this, message);
		},
		hideDeleted: function(callback) {
			UI.hideDeleted(this, callback);
		},
		
		showContact: function(contact) {
			UI.showContact(this, contact);
		}
		
	}
};