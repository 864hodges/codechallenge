var ContactAddEditViewMock = function() {
	
	var UI = {
		
		showWaiting: function(self) {
		},		
		showSaving: function(self) {
		},
		hideSaving: function(self) {
		},
		showSaved: function(self, message) {
		},
		hideSaved: function(self, callback) {
		},
		
		populateContactForm: function(self, contact) {
		},
		
		populateContactObject: function(self) {
			var c = Contact();			
			c.id = 15;
			
			c.first_name = 'First15';
			c.last_name = 'Last15';
			c.company_name = 'Company Name15';

			c.address = 'Street Address15';
			c.city = 'City15';
			c.state = 'State15';
			c.zip = 'Zip Code15';

			c.phone = 'Phone15';
			c.work_phone = '';
			c.email = '';
			c.url = '';
			
			return c;
		},
		
		clearForm: function(self) {
		},
		
		isFormValid: function(self) {
			return true;
		}
		
	};
		
	return {
		
		eventContext: null,
		
		init: function(eventContext) {
			var self = this;
			this.eventContext = eventContext;
		},
		
		showWaiting: function() {
			UI.showWaiting(this);
		},
		showSaving: function() {
			UI.showSaving(this);	
		},
		hideSaving: function() {
			UI.hideSaving(this);
		},
		showSaved: function(message) {
			UI.showSaved(this, message);	
		},
		hideSaved: function(callback) {
			UI.hideSaved(this, callback);
		},
		
		populateContactForm: function(contact) {
			UI.populateContactForm(this, contact);
		}
		
	}
};