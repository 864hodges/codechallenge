///
/// The ContactAddEditView class is here to maninpulate the view and retrieve data from the user.
///
/// Auther: Mike Hodges
/// Created: 9/20/2015
///
var ContactAddEditView = function() {
	
	///
	/// This section manipulates the DOM.
	///
	var UI = {
		///
		/// This method shows the user that the system is busy.
		///
		showWaiting: function(self) {
			self.contactDisplayArea.parent().find('.profileimage').show();
			self.contactDisplayArea.hide();
		},
		
		///
		/// This method shows the user that the system is saving changes.
		///
		showSaving: function(self) {
			$('.saving').show();
		},
		///
		/// This method removes the saving changes notice.
		///
		hideSaving: function(self) {
			$('.saving').hide();
		},
		///
		/// This method shows the user that the system has successfully saved the changes.
		///
		showSaved: function(self, message) {
			if (message) {
				$('.saved').html(message).show();
			} else {
				$('.saved').show();
			}	
		},
		///
		/// This method removes the successful save notice.
		///
		hideSaved: function(self, callback) {
			$('.saved').hide( "drop", { direction: "down" }, 5000, function() { callback(); });
		},
		
		///
		/// This method populates the form to allow the user to edit the contact information.
		///
		populateContactForm: function(self, contact) {
			self.contactDisplayArea.parent().find('.profileimage').hide('slow');
			
			$('h1:first', self.contactDisplayArea).attr('prop-id', contact.id);
			
			$('#txtFirstName', self.contactDisplayArea).val(contact.first_name);
			$('#txtLastName', self.contactDisplayArea).val(contact.last_name);
			$('#txtCompanyName', self.contactDisplayArea).val(contact.company_name);

			$('#txtAddress', self.contactDisplayArea).val(contact.address);
			$('#txtCity', self.contactDisplayArea).val(contact.city);
			$('#txtState', self.contactDisplayArea).val(contact.state);
			$('#txtZip', self.contactDisplayArea).val(contact.zip);

			$('#txtPhone', self.contactDisplayArea).val(contact.phone);
			$('#txtWorkPhone', self.contactDisplayArea).val(contact.work_phone);
			$('#txtEmail', self.contactDisplayArea).val(contact.email);
			$('#txtURL', self.contactDisplayArea).val(contact.url);
			
			self.contactDisplayArea.show('slow');
		},
		
		///
		/// This method populates a Contact object with data from the form.
		///
		populateContactObject: function(self) {
			var c = Contact();
			c.id = parseInt($('h1:first', self.contactDisplayArea).attr('prop-id'));
			
			c.first_name = $('#txtFirstName', self.contactDisplayArea).val() ? $('#txtFirstName', self.contactDisplayArea).val().trim() : null;
			c.last_name = $('#txtLastName', self.contactDisplayArea).val().trim();
			c.company_name = $('#txtCompanyName', self.contactDisplayArea).val() ? $('#txtCompanyName', self.contactDisplayArea).val().trim() : null;

			c.address = $('#txtAddress', self.contactDisplayArea).val() ? $('#txtAddress', self.contactDisplayArea).val().trim() : null;
			c.city = $('#txtCity', self.contactDisplayArea).val() ? $('#txtCity', self.contactDisplayArea).val().trim() : null;
			c.state = $('#txtState', self.contactDisplayArea).val() ? $('#txtState', self.contactDisplayArea).val().trim() : null;
			c.zip = $('#txtZip', self.contactDisplayArea).val() ? $('#txtZip', self.contactDisplayArea).val().trim() : null;
			c.zip = (c.zip === "0" || c.zip === 0) ? null : c.zip;

			c.phone = $('#txtPhone', self.contactDisplayArea).val() ? $('#txtPhone', self.contactDisplayArea).val().trim() : null;
			c.work_phone = $('#txtWorkPhone', self.contactDisplayArea).val() ? $('#txtWorkPhone', self.contactDisplayArea).val().trim() : null;
			c.email = $('#txtEmail', self.contactDisplayArea).val() ? $('#txtEmail', self.contactDisplayArea).val().trim() : null;
			c.url = $('#txtURL', self.contactDisplayArea).val() ? $('#txtURL', self.contactDisplayArea).val().trim() : null;
			
			return c;
		},
		
		///
		/// This method clears the form.
		///
		clearForm: function(self) {
			$('h1:first', self.contactDisplayArea).removeAttr('prop-id');
			
			$('#txtFirstName', self.contactDisplayArea).val('');
			$('#txtLastName', self.contactDisplayArea).val('');
			$('#txtCompanyName', self.contactDisplayArea).val('');

			$('#txtAddress', self.contactDisplayArea).val('');
			$('#txtCity', self.contactDisplayArea).val('');
			$('#txtState', self.contactDisplayArea).val('');
			$('#txtZip', self.contactDisplayArea).val('');

			$('#txtPhone', self.contactDisplayArea).val('');
			$('#txtWorkPhone', self.contactDisplayArea).val('');
			$('#txtEmail', self.contactDisplayArea).val('');
			$('#txtURL', self.contactDisplayArea).val('');			
		},
		
		///
		/// This method validates that the required fields have a value.
		///
		isFormValid: function(self) {
			var first_name = $('#txtFirstName', self.contactDisplayArea).val();
			var last_name = $('#txtLastName', self.contactDisplayArea).val();
			
			if (first_name.trim().length === 0 || last_name.trim().length === 0) { return false; }
			return true;
		}
	};
		
	return {
		eventContext: null,
		contactDisplayArea: null,
		
		///
		/// This method initializes the view instance by accepting refences to its dependencies
		/// and listening for pertinent events.
		///
		init: function(eventContext, displayArea) {
			var self = this;
			this.eventContext = eventContext;
			this.contactDisplayArea = displayArea;
			
			// Set focus in the first name textbox
			$('#txtFirstName', self.contactDisplayArea).focus();
			
			// Listen for user interaction events
			$('#saveContact', self.contactDisplayArea).bind('click', function() {
				if (UI.isFormValid(self)) {
					var contact = UI.populateContactObject(self);
					
					var event = $.Event('saveContactClicked', { contact: contact });
					self.eventContext.trigger(event);
				} else {
					alert('You must provide at least your name and phone number.');
				}
			});
			$('#cancelSaveContact', self.contactDisplayArea).bind('click', function() {
				UI.clearForm(self);
				
				var event = $.Event('cancelSaveContactClicked');
				self.eventContext.trigger(event);
			});
			$('#logo').bind('click', function() {
				window.location = "index.html";
			});
			
			// Listen for events that provide data
			self.eventContext.bind('contactSelectedForEdit', function(contact) {
				UI.populateContactForm(self, contact);
			});
		},
		
		///
		/// This method shows the user that the system is busy.
		///
		showWaiting: function() {
			UI.showWaiting(this);
		},
		///
		/// This method shows the user that the system is saving changes.
		///
		showSaving: function() {
			UI.showSaving(this);	
		},
		///
		/// This method removes the saving changes notice.
		///
		hideSaving: function() {
			UI.hideSaving(this);
		},
		///
		/// This method shows the user that the system has successfully saved the changes.
		///
		showSaved: function(message) {
			UI.showSaved(this, message);	
		},
		///
		/// This method removes the successful save notice.
		///
		hideSaved: function(callback) {
			UI.hideSaved(this, callback);
		},
		
		///
		/// This method populates the form to allow the user to edit the contact information.
		///
		populateContactForm: function(contact) {
			UI.populateContactForm(this, contact);
		}
	}
};