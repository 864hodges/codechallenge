///
/// The ContactListView class is here to maninpulate the view and retrieve data from the user.
///
/// Auther: Mike Hodges
/// Created: 9/18/2015
///
var ContactListView = function() {
	
	///
	/// This section manipulates the DOM.
	///
	var UI = {
		///
		/// This method shows the user that the system is busy.
		///
		showWaiting: function(self) {
			var waiting = '<div class="container-list-bottom">';
			waiting += '  <div class="container-list-top">';
			waiting += '    <div class="container-list-middle">';
			waiting += '      <div id="home" class="visible section">';
			waiting += '        <div class="profileinfo-list">';
			waiting += '          <img src="images/wait.gif" />';
			waiting += '        </div>';
			waiting += '        <div class="cb"></div>';
			waiting += '      </div>';
			waiting += '    </div>';
			waiting += '  </div>';
			waiting += '</div>';
			waiting += '<div class="shadow-list"></div>';

			self.listDisplayArea.html($('<div/>').html(waiting).html());
		},

		///
		/// This method shows the user that the system has encountered an error.
		///
		showError(self, errorMsg) {
			$('.errorMessage').html(errorMsg);
			$('.error').show();
		},
		///
		/// This method removes the error notice.
		///
		hideError(self) {
			$('.error').hide();
			$('.errorMessage').html('');
		},
		
		///
		/// This method shows the list of contacts.
		///
		showList: function(self, contactResults) {
			var contactList = contactResults.data;
			UI.buildNavigation(self, contactResults);
			
			var counter = 0, items = '';
			for (counter; counter < contactList.length; counter++) {
				items += '<div class="container-list-bottom" data-id="' + contactList[counter].id + '">';
				items += '  <div class="container-list-top">';
				items += '    <div class="container-list-middle">';
				items += '      <div id="home" class="visible section">';
				items += '        <div class="profileinfo-list">';
				items += '          <h1>' + contactList[counter].first_name + ' ' + contactList[counter].last_name + '</h1>';
				items += '          <span class="company-list">' + contactList[counter].company_name + '</span>';
				items += '          <div class="cb"></div>';
				items += '        </div>';
				items += '        <div class="cb"></div>';
				items += '      </div>';
				items += '    </div>';
				items += '  </div>';
				items += '</div>';
				items += '<div class="shadow-list"></div>';
			}						
						
			self.listDisplayArea.html($('<div/>').html(items).html());
			UI.markSelected(self, self.listDisplayArea.find('.container-list-bottom:first'));
			
			$('.container-list-bottom', self.listDisplayArea).on('click', function() {
				UI.markSelected(self, $(this));
				
				var event = $.Event('contactSelectedFromList', { id: $(this).attr('data-id')});
				self.eventContext.trigger(event);
			});
		},
		
		///
		/// This method builds the navigation section above the contact list.
		///
		buildNavigation: function(self, contactResults) {
			// Build the info section
			var info = '<span>Showing ' + contactResults.from + ' to ' + contactResults.to + ' of ' + contactResults.total + '</span>';
			self.infoDisplayArea.html(info);

			// Build the paging section
			var paging = '<select id="numPerPage">';
			paging += '<option value="10">10</option>';
			paging += '<option value="25">25</option>';
			paging += '<option value="50">50</option>';
			paging += '</select>';
			
			if (contactResults.next_page_url !== null) { paging += '<button id="next" style="float:right">Next &#062;</button>'; }			
			if (contactResults.prev_page_url !== null) { paging += '<button id="prev" style="float:right">&#060; Prev</button>'; }
			self.pagingDisplayArea.html($('<div/>').html(paging).html());

			$('option[value="' + contactResults.per_page + '"]', self.pagingDisplayArea).attr('selected', true);		
			
			// Build the sorting section
			var sorting = ''; sortVal = self.parentController.getSetting('sort');
			sorting += '<button id="sortByDate" class="sorter ' + ((sortVal === 'created_at')?'sorted':'') + '">Date</button>';
			sorting += '<button id="sortByLastName" class="sorter ' + ((sortVal === 'last_name')?'sorted':'') + '">Last Name</button>';
			sorting += '<button id="sortByFirstName" class="sorter ' + ((sortVal === 'first_name')?'sorted':'') + '">First Name</button>';
			self.sortingDisplayArea.html($('<div/>').html(sorting).html());
			
			// Send events when the user does something
			$('#numPerPage', self.pagingDisplayArea).on('change', function() {
				var event = $.Event('listSizeChanged', { size: $(this).val()});
				self.eventContext.trigger(event);
			});
			$('#next', self.pagingDisplayArea).on('click', function() {
				var event = $.Event('nextPageClicked');
				self.eventContext.trigger(event);
			});
			$('#prev', self.pagingDisplayArea).on('click', function() {
				var event = $.Event('prevPageClicked');
				self.eventContext.trigger(event);
			});
			$('#sortByDate', self.sortingDisplayArea).on('click', function() {
				$('.sorter', self.sortingDisplayArea).removeClass('sorted');
				$(this).addClass('sorted');
				
				var event = $.Event('sortButtonClicked', { sort: 'date' });
				self.eventContext.trigger(event);
			});
			$('#sortByLastName', self.sortingDisplayArea).on('click', function() {
				$('.sorter', self.sortingDisplayArea).removeClass('sorted');
				$(this).addClass('sorted');
				
				var event = $.Event('sortButtonClicked', { sort: 'lastName' });
				self.eventContext.trigger(event);
			});
			$('#sortByFirstName', self.sortingDisplayArea).on('click', function() {
				$('.sorter', self.sortingDisplayArea).removeClass('sorted');
				$(this).addClass('sorted');
				
				var event = $.Event('sortButtonClicked', { sort: 'firstName' });
				self.eventContext.trigger(event);
			});
		},
		
		///
		/// This method shows the user that a contact has been selected in the list.
		///
		markSelected: function(self, container) {
			self.listDisplayArea.find('.selected').removeClass('selected');
			
			container.addClass('selected');
			container.find('.container-list-top, .container-list-middle').addClass('selected');
		}
	};
		
	return {
		parentController: null,
		eventContext: null,
		listDisplayArea: null,
		infoDisplayArea: null,
		pagingDisplayArea: null,
		sortingDisplayArea: null,
		
		///
		/// This method initializes the view instance by accepting refences to its dependencies
		/// and listening for pertinent events.
		///
		init: function(controller, eventContext, listArea, infoArea, pagingArea, sortArea) {
			var self = this;
			this.parentController = controller;
			this.eventContext = eventContext;
			this.listDisplayArea = listArea;
			this.infoDisplayArea = infoArea;
			this.pagingDisplayArea = pagingArea;
			this.sortingDisplayArea = sortArea;
			
			// Listen for user interaction events
			$('#addContact').bind('click', function() {
				var event = $.Event('addContactClicked');
				self.eventContext.trigger(event);				
			});
			$('#logo').bind('click', function() {
				window.location = "index.html";
			});
			$('.dismissError').bind('click', function() {
				UI.hideError(self);
			});
			
			// Listen for events that provide data
			self.eventContext.bind('contactListReceived', function(event) {
				UI.showList(self, event.results);
			});
		},
		
		///
		/// This method shows the user that the system is busy.
		///
		showWaiting: function() {
			UI.showWaiting(this);
		},
		
		///
		/// This method shows the user that the system has encountered an error.
		///
		showError: function(errorMsg) {
			UI.showError(this, errorMsg);
		},
		///
		/// This method removes the error notice.
		///
		hideError: function() {
			UI.hideError(this);
		},
		
		///
		/// This method shows the list of contacts.
		///
		showList: function(contactResults) {
			UI.showList(this, contactResults);
		}
	}
};