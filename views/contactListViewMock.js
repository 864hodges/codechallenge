var ContactListViewMock = function() {
	
	var UI = {
		
		showWaiting: function(self) {
		},
		showError(self, errorMsg) {
		},
		hideError(self) {
		},
		
		showList: function(self, contactResults) {
		},
		
		buildNavigation: function(self, contactResults) {
		},
		
		markSelected: function(self, container) {
		}
		
	};
		
	return {
		
		parentController: null,
		eventContext: null,
		
		init: function(controller, eventContext) {
			var self = this;
			this.parentController = controller;
			this.eventContext = eventContext;
		},
		
		showWaiting: function() {
			UI.showWaiting(this);
		},
		
		showList: function(contactResults) {
			UI.showList(this, contactResults);
		},
		
		showError: function(errorMsg) {
			UI.showError(this, errorMsg);
		},
		hideError: function() {
			UI.hideError(this);
		}
		
	}
};