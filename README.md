# README #

Hi and thank you again for giving me the opportunity to demonstrate my JavaScript skills and I hope you like what you see. If not, please let me know what I can do to improve the solution.

### Things to note: ###
* I used dependency injection and events to reduce coupling and make unit testing easier.
* I unit tested the data access and controller components. The views were mocked to allow the controller to be tested.
* The data component unit tests currently have the add/edit/delete tests commented out. This is because, they cause the other tests to fail. The other tests are written to expect 50 results. I ran out of time to correct this.
* The contact list on the left side of the screen does not have a complete pagination system in place. For example, you cannot go directly to page 3 - you must go page by page.
* Below the list length selector is a row of three sort options. 'Date' sorts by created_at descending, 'Last Name' and 'First Name' sort by last_name and first_name respectively, both ascending.
* I started with a site design from freewebtemplates.com. I did that, because my site design skills are not strong and to save time. Here's a link to the original design (http://www.freewebtemplates.com/download/free-website-template/virtual-card-template-854621212/).

### Getting started: ###
* index.html is the main page for the site, as the directions stipulate.
* Loading tests.html in your browser will execute the unit tests.

### Assumptions: ###
* I assumed that you did not need to see me exercise each path through the web services.

### 3rd party libraries/frameworks: ###
* jQuery v1.11.3
* jQueryUI v1.11.4
* QUnit v1.19.0