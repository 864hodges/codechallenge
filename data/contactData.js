///
/// The ContactData class is here to call the web services and get and save contacts.
///
/// Auther: Mike Hodges
/// Created: 9/18/2015
///
var ContactData = function() {
	
	///
	/// This section contains the contants.
	///
	var Constants = {
		'X_AUTH_TOKEN': 'EnbZPTTKtZiwxfMqScT2QAK3Swapx4QNZVAFY9dk',
		'URL_BASE': 'http://challenge.acstechnologies.com/api'
	};
	
	///
	/// This section contains methods that call the services.
	///
	var Data = {
		///
		/// This method gets the contact list from the service, resolves the promise object,
		/// and then returns the promise.
		///
		getContactListDFD: function(limit, sort, desc, page) {
			var gettingList = new $.Deferred();
			
			$.ajax({
				type: 'GET',
				cache: false,
				url: Constants.URL_BASE + '/contact/',
				headers: {
					'X-Auth-Token': Constants.X_AUTH_TOKEN
				},
				data: {
					'limit': limit,
					'sort': sort,
					'desc': desc,
					'page': page	
				},
				success: function(data, textStatus, jqXHR) {
					// Save the returned data. It will be returned in the promise object.
					gettingList.resolve(data);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					if (jqXHR.status === 0) {
						gettingList.reject(textStatus);
					} else {
						gettingList.reject(jqXHR.status + ': ' + jqXHR.statusText + '<br/>' + jqXHR.responseText);
					}
				}
			});
			
			return gettingList.promise();
		},
		
		///
		/// This method gets a specific contact from the service by id, resolves the promise object,
		/// and then returns the promise.
		///
		getContactByIDDFD: function(contactID) {
			var gettingContact = new $.Deferred();
			
			$.ajax({
				type: 'GET',
				cache: false,
				url: Constants.URL_BASE + '/contact/' + contactID,
				headers: {
					'X-Auth-Token': Constants.X_AUTH_TOKEN
				},
				success: function(data, textStatus, jqXHR) {
					// If the zip code was not provided in the contact, 0 will be returned. Let's clean that up.
					data.zip = data.zip === 0 ? '' : data.zip;
					
					// Save the returned data. It will be returned in the promise object.
					gettingContact.resolve(data);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					if (jqXHR.status === 0) {
						gettingContact.reject(textStatus);
					} else {
						gettingContact.reject(jqXHR.status + ': ' + jqXHR.statusText + '<br/>' + jqXHR.responseText);
					}
				}
			});
			
			return gettingContact.promise();
		},
		
		///
		/// This method adds a contact to the service, resolves the promise object,
		/// and then returns the promise.
		///
		addContactDFD: function(contact) {
			var savingContact = new $.Deferred();
			
			$.ajax({
				type: 'POST',
				cache: false,
				url: Constants.URL_BASE + '/contact/',
				headers: {
					'X-Auth-Token': Constants.X_AUTH_TOKEN
				},
				data: {
					'first_name': contact.first_name,
					'last_name': contact.last_name,
					'company_name': contact.company_name,
					'address': contact.address,
					'city': contact.city,
					'state': contact.state,
					'zip': contact.zip,
					'phone': contact.phone,
					'work_phone': contact.work_phone,
					'email': contact.email,
					'url': contact.url	
				},
				success: function(data, textStatus, jqXHR) {
					// Make sure I'm not sending a 0 for an empty zip code
					if (data.success) {
						data.new_contact.zip = data.new_contact.zip === 0 ? '' : data.new_contact.zip;
					}
					
					// Save the returned data (new_contact). It will be returned in the promise object.
					savingContact.resolve(data);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					if (jqXHR.status === 0) {
						savingContact.reject(textStatus);
					} else {
						savingContact.reject(jqXHR.status + ': ' + jqXHR.statusText + '<br/>' + jqXHR.responseText);
					}
				}
			});
			
			return savingContact.promise();
		},
		
		///
		/// This method modifies a contact with the service, resolves the promise object,
		/// and then returns the promise.
		///
		editContactDFD: function(contact) {
			var savingContact = new $.Deferred();
			
			$.ajax({
				type: 'PUT',
				cache: false,
				url: Constants.URL_BASE + '/contact/' + contact.id,
				headers: {
					'X-Auth-Token': Constants.X_AUTH_TOKEN
				},
				data: {
					'first_name': contact.first_name,
					'last_name': contact.last_name,
					'company_name': contact.company_name,
					'address': contact.address,
					'city': contact.city,
					'state': contact.state,
					'zip': contact.zip,
					'phone': contact.phone,
					'work_phone': contact.work_phone,
					'email': contact.email,
					'url': contact.url	
				},
				success: function(data, textStatus, jqXHR) {
					// Make sure I'm not sending a 0 for an empty zip code
					if (data.success) {
						data.updated_contact.zip = data.updated_contact.zip === 0 ? '' : data.updated_contact.zip;
					}
					
					// Save the returned data (udpated_contact). It will be returned in the promise object.
					savingContact.resolve(data);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					if (jqXHR.status === 0) {
						savingContact.reject(textStatus);
					} else {
						savingContact.reject(jqXHR.status + ': ' + jqXHR.statusText + '<br/>' + jqXHR.responseText);
					}
				}
			});
			
			return savingContact.promise();
		},
		
		///
		/// This method deletes a contact with the service, resolves the promise object,
		/// and then returns the promise.
		///
		deleteContactDFD: function(contactID) {
			var deletingContact = new $.Deferred();
			
			$.ajax({
				type: 'DELETE',
				cache: false,
				url: Constants.URL_BASE + '/contact/' + contactID,
				headers: {
					'X-Auth-Token': Constants.X_AUTH_TOKEN
				},
				success: function(data, textStatus, jqXHR) {
					// Save the returned data (success flag). It will be returned in the promise object.
					deletingContact.resolve(data);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					if (jqXHR.status === 0) {
						deletingContact.reject(textStatus);
					} else {
						deletingContact.reject(jqXHR.status + ': ' + jqXHR.statusText + '<br/>' + jqXHR.responseText);
					}
				}
			});
			
			return deletingContact.promise();
		}
						
	};
			
	return {			
		///
		/// This method gets the contact list from the service, resolves the promise object,
		/// and then returns the promise.
		///
		getContactListDFD: function(limit, sort, desc, page) {
			if ((limit && (isNaN(limit))) || (page && isNaN(page))) {
				var gettingList = new $.Deferred();
				gettingList.reject('The limit and page are integers. Please provide valid integer values');
				return gettingList.promise();
			}
			
			limit = parseInt(limit || 10);
			sort = sort || 'last_name';
			desc = desc || false;
			page = parseInt(page || 1);
											
			return Data.getContactListDFD(limit, sort, desc, page);
		},
		
		///
		/// This method gets a specific contact from the service by id, resolves the promise object,
		/// and then returns the promise.
		///
		getContactByIDDFD: function(contactID) {
			return Data.getContactByIDDFD(contactID);
		},
		
		///
		/// This method calls either the editContactDFD or the addContactDFD method.
		///
		saveContactDFD: function(contact) {
			if (contact.id) {
				return Data.editContactDFD(contact);
			} else {
				return Data.addContactDFD(contact);
			}
		},
		
		///
		/// This method deletes a contact with the service, resolves the promise object,
		/// and then returns the promise.
		///
		deleteContactDFD: function(contactID) {
			return Data.deleteContactDFD(contactID);
		}
	}
};