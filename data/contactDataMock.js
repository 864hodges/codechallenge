var ContactDataMock = function() {
	
	var TestData = {
		
		giveContacts: function(numberToReturn, page) {
			var data = [];
			
			var counter = (page === 1)?0:numberToReturn * (page-1);
			var max = numberToReturn+(numberToReturn * (page-1));
			for (counter; counter < max; counter++) {
				var _id = counter+1;
				
				var c = Contact();
				c.id = _id;
				c.first_name = "First" + _id;
				c.last_name = "Last" + _id;
				c.company_name = "Company" + _id;
				c.address = "Street Address" + _id;
				c.city = "City" + _id;
				c.state = "State" + _id;
				c.zip = "Zip" + _id;
				c.phone = "Phone" + _id;
				c.work_phone = "WorkPhone" + _id;
				c.email = "EmailAddress" + _id;
				c.url = "URL" + _id;
				c.created_at = new Date();
				c.updated_at = new Date();
				data[data.length] = c;
			}
			
			return data;
		},
		
		giveContact: function(contactID) {
			var c = Contact();
			c.id = contactID;
			c.first_name = "First" + contactID;
			c.last_name = "Last" + contactID;
			c.company_name = "Company" + contactID;
			c.address = "Street Address" + contactID;
			c.city = "City" + contactID;
			c.state = "State" + contactID;
			c.zip = "Zip" + contactID;
			c.phone = "Phone" + contactID;
			c.work_phone = "WorkPhone" + contactID;
			c.email = "EmailAddress" + contactID;
			c.url = "http://valid" + contactID + ".url.com";
			c.created_at = new Date();
			c.updated_at = new Date();
			return c;
		}
		
	};
	
	var Data = {
		
		getContactListDFD: function(limit, sort, desc, page) {
			var gettingList = new $.Deferred();
			page = page || 1;
			
			var data = {
				total: 50,
				from: (page === 1)?1:limit*(page-1),
				to: limit*page,
				last_page: 50/limit,
				next_page_url: ((50-(limit*page))>0)?'http://valid.url.com':null,
				per_page: "" + limit,
				prev_page_url: (page>1)?'http://valid.url.com':null,
				data: TestData.giveContacts(limit, page)
			};
			gettingList.resolve(data);
			
			return gettingList.promise();
		},
		
		getContactByIDDFD: function(contactID) {
			var gettingContact = new $.Deferred();
			
			var data = TestData.giveContact(contactID);
			gettingContact.resolve(data);
			
			return gettingContact.promise();
		},
		
		addContactDFD: function(contact) {
			var gettingContact = new $.Deferred();
			
			var data = {
				success: true,
				new_contact: contact
			}
			gettingContact.resolve(data);
			
			return gettingContact.promise();
		},
		
		editContactDFD: function(contact) {
			var gettingContact = new $.Deferred();
			
			var data = {
				success: true,
				updated_contact: contact
			}
			gettingContact.resolve(data);
			
			return gettingContact.promise();
		},
				
		deleteContactDFD: function(contactID) {
			var gettingContact = new $.Deferred();
			
			var data = {
				success: true
			}
			gettingContact.resolve(data);
			
			return gettingContact.promise();
		}
		
	};
			
	return {			
		
		getContactListDFD: function(limit, sort, desc, page) {
			limit = parseInt(limit);
			return Data.getContactListDFD(limit, sort, desc, page);
		},
		
		getContactByIDDFD: function(contactID) {
			return Data.getContactByIDDFD(contactID);
		},
		
		saveContactDFD: function(contact) {
			if (contact.id) {
				return Data.editContactDFD(contact);
			} else {
				return Data.addContactDFD(contact);
			}
		},
		
		deleteContactDFD: function(contactID) {
			return Data.deleteContactDFD(contactID);
		}
		
	}
};