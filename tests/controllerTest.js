QUnit.test('Test getContactList', function(assert) {
	var contactData = ContactDataMock();	
	var contactListView = ContactListViewMock();
	var contactDetailView = ContactDetailViewMock();
	var contactAddEditView = ContactAddEditViewMock();
	var contactCtrl = ContactController();
	
	var eventContext = $('#qunit');
	contactListView.init(contactCtrl, eventContext);
	contactDetailView.init(eventContext);
	contactAddEditView.init(eventContext);
	contactCtrl.init(eventContext, contactData, contactListView, contactDetailView, contactAddEditView);
	
	eventContext.bind('contactListReceived', function(event) {
		var results = event.results;
		assert.deepEqual(results.total, 50, 'The total number of contacts should be 50');
		assert.deepEqual(results.from, 1, 'Should be on the first page');
		assert.deepEqual(results.to, 10, 'Should be 10 contacts per page');
		assert.deepEqual(results.last_page, 5, 'There should be 5 pages');
		assert.ok(results.next_page_url, 'There next page URL should not be null');
		assert.deepEqual(results.per_page, "10", 'There should be 10 contacts per pages');
		assert.notOk(results.prev_page_url, 'There prev page URL should be null');
		assert.ok(results.data, 'An array of contacts should be provided');
		assert.deepEqual(results.data.length, 10, 'Should be 10 contacts in the array');
	});	
	eventContext.bind('contactSelectedForDisplay', function(contact) {
		assert.deepEqual(contact.id, 1, 'The returned contact ID should be 1');
		assert.deepEqual(contact.first_name, 'First1', 'The first name should be First1');
	});	
	
	contactCtrl.clearSettings();
	var contacts = contactCtrl.getContactList(10, 'last_name', false, 1);
});

QUnit.test('Test contactSelectedFromList', function(assert) {
	var contactData = ContactDataMock();	
	var contactListView = ContactListViewMock();
	var contactDetailView = ContactDetailViewMock();
	var contactAddEditView = ContactAddEditViewMock();
	var contactCtrl = ContactController();
	
	var eventContext = $('#qunit');
	contactListView.init(contactCtrl, eventContext);
	contactDetailView.init(eventContext);
	contactAddEditView.init(eventContext);
	contactCtrl.init(eventContext, contactData, contactListView, contactDetailView, contactAddEditView);
	
	eventContext.bind('contactSelectedForDisplay', function(contact) {
		assert.deepEqual(contact.id, 2, 'The returned contact ID should be 2');
		assert.deepEqual(contact.first_name, 'First2', 'The first name should be First2');
	});	
	
	contactCtrl.clearSettings();
	var contacts = contactCtrl.contactSelectedFromList(2);
});

QUnit.test('Test contactSelectedForEdit', function(assert) {
	var contactData = ContactDataMock();	
	var contactListView = ContactListViewMock();
	var contactDetailView = ContactDetailViewMock();
	var contactAddEditView = ContactAddEditViewMock();
	var contactCtrl = ContactController();
	
	var eventContext = $('#qunit');
	contactListView.init(contactCtrl, eventContext);
	contactDetailView.init(eventContext);
	contactAddEditView.init(eventContext);
	contactCtrl.init(eventContext, contactData, contactListView, contactDetailView, contactAddEditView);
	
	eventContext.bind('contactSelectedForEdit', function(contact) {
		assert.deepEqual(contact.id, 4, 'The returned contact ID should be 4');
		assert.deepEqual(contact.first_name, 'First4', 'The first name should be First4');
	});	
	
	contactCtrl.clearSettings();
	var contacts = contactCtrl.contactSelectedForEdit(4);
});

QUnit.test('Test saveSetting / getSetting', function(assert) {
	var contactData = ContactDataMock();	
	var contactListView = ContactListViewMock();
	var contactDetailView = ContactDetailViewMock();
	var contactAddEditView = ContactAddEditViewMock();
	var contactCtrl = ContactController();
	
	var eventContext = $('#qunit');
	contactListView.init(contactCtrl, eventContext);
	contactDetailView.init(eventContext);
	contactAddEditView.init(eventContext);
	contactCtrl.init(eventContext, contactData, contactListView, contactDetailView, contactAddEditView);
	
	contactCtrl.saveSetting('testKey', 'testValue');
	assert.deepEqual(contactCtrl.getSetting('testKey'), 'testValue', 'getSetting should return simple string');
	
	contactCtrl.saveSetting('testKey2', { key: 'value' });
	assert.equal(contactCtrl.getSetting('testKey2'), { key: 'value' }, 'getSetting should return complex object');
});
