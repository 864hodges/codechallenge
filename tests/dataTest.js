QUnit.test('Test Getting Contact List from Server (Defaults)', function(assert) {
	var data = ContactData();
	
	var done = assert.async();
	data.getContactListDFD()
		.done(function(results) {
			assert.ok(true, 'Successfully got data');
			assert.deepEqual(results.total, 50, 'There should be a total of 50 records available')
			assert.deepEqual(results.from, 1, 'Should be on the first page');
			assert.deepEqual(results.to, 10, 'Should be 10 contacts per page');
			assert.deepEqual(results.last_page, 5, 'There should be 5 pages');
			assert.ok(results.next_page_url, 'There next page URL should not be null');
			assert.deepEqual(results.per_page, "10", 'There should be 10 contacts per pages');
			assert.notOk(results.prev_page_url, 'There prev page URL should be null');
			assert.ok(results.data, 'An array of contacts should be provided');
			assert.deepEqual(results.data.length, 10, 'Should be 10 contacts in the array');
		})
		.fail(function(error) {
			assert.ok(false, 'Got error: ' + error);
		})
		.always(function() {
			done();	
		});
});

QUnit.test('Test Getting Contact List from Server (Limit 20)', function(assert) {
	var data = ContactData();
	
	var done = assert.async();
	data.getContactListDFD(20)
		.done(function(results) {
			assert.ok(true, 'Successfully got data');
			assert.deepEqual(results.total, 50, 'There should be a total of 50 records available')
			assert.deepEqual(results.from, 1, 'Should be on the first page');
			assert.deepEqual(results.to, 20, 'Should be 20 contacts per page');
			assert.deepEqual(results.last_page, 3, 'There should be 5 pages');
			assert.ok(results.next_page_url, 'There next page URL should not be null');
			assert.deepEqual(results.per_page, "20", 'There should be 20 contacts per pages');
			assert.notOk(results.prev_page_url, 'There prev page URL should be null');
			assert.ok(results.data, 'An array of contacts should be provided');
			assert.deepEqual(results.data.length, 20, 'Should be 20 contacts in the array');
		})
		.fail(function(error) {
			assert.ok(false, 'Got error: ' + error);
		})
		.always(function() {
			done();	
		});
});

QUnit.test('Test Getting Contact List from Server (Sorting)', function(assert) {
	var data = ContactData();
	
	var done = assert.async();
	data.getContactListDFD(10, 'first_name')
		.done(function(results) {
			assert.ok(true, 'Successfully got data');
			assert.deepEqual(results.total, 50, 'There should be a total of 50 records available')
			assert.deepEqual(results.from, 1, 'Should be on the first page');
			assert.deepEqual(results.to, 10, 'Should be 10 contacts per page');
			assert.deepEqual(results.last_page, 5, 'There should be 5 pages');
			assert.ok(results.next_page_url, 'There next page URL should not be null');
			assert.deepEqual(results.per_page, "10", 'There should be 10 contacts per pages');
			assert.notOk(results.prev_page_url, 'There prev page URL should be null');
			assert.ok(results.data, 'An array of contacts should be provided');
			assert.deepEqual(results.data.length, 10, 'Should be 10 contacts in the array');			
			assert.deepEqual(results.data[0].first_name, 'Aiden', 'Aiden Bechelar should be first sored ascending')
		})
		.fail(function(error) {
			assert.ok(false, 'Got error: ' + error);
		})
		.always(function() {
			done();	
		});
});

QUnit.test('Test Getting Contact List from Server (Sorting Descending)', function(assert) {
	var data = ContactData();
	
	var done = assert.async();
	data.getContactListDFD(10, 'first_name', true)
		.done(function(results) {
			assert.ok(true, 'Successfully got data');
			assert.deepEqual(results.total, 50, 'There should be a total of 50 records available')
			assert.deepEqual(results.from, 1, 'Should be on the first page');
			assert.deepEqual(results.to, 10, 'Should be 10 contacts per page');
			assert.deepEqual(results.last_page, 5, 'There should be 5 pages');
			assert.ok(results.next_page_url, 'There next page URL should not be null');
			assert.deepEqual(results.per_page, "10", 'There should be 10 contacts per pages');
			assert.notOk(results.prev_page_url, 'There prev page URL should be null');
			assert.ok(results.data, 'An array of contacts should be provided');
			assert.deepEqual(results.data.length, 10, 'Should be 10 contacts in the array');
			assert.deepEqual(results.data[0].first_name, 'Verlie', 'Verlie Bosco should be first sored ascending')
		})
		.fail(function(error) {
			assert.ok(false, 'Got error: ' + error);
		})
		.always(function() {
			done();	
		});
});

QUnit.test('Test Getting Contact List from Server (Page 2)', function(assert) {
	var data = ContactData();
	
	var done = assert.async();
	data.getContactListDFD(10, 'first_name', true, 2)
		.done(function(results) {
			assert.ok(true, 'Successfully got data');
			assert.deepEqual(results.total, 50, 'There should be a total of 50 records available')
			assert.deepEqual(results.from, 11, 'Should be on the first page');
			assert.deepEqual(results.to, 20, 'Should be 20 contacts per page');
			assert.deepEqual(results.last_page, 5, 'There should be 5 pages');
			assert.ok(results.next_page_url, 'There next page URL should not be null');
			assert.deepEqual(results.per_page, "10", 'There should be 10 contacts per pages');
			assert.ok(results.prev_page_url, 'There prev page URL should not be null');
			assert.ok(results.data, 'An array of contacts should be provided');
			assert.deepEqual(results.data.length, 10, 'Should be 10 contacts in the array');
			assert.deepEqual(results.data[0].first_name, 'Myron', 'Myron Schowalter should be first sored ascending')
		})
		.fail(function(error) {
			assert.ok(false, 'Got error: ' + error);
		})
		.always(function() {
			done();	
		});
});

QUnit.test('Test Getting Contact List from Server (limit = "10")', function(assert) {
	var data = ContactData();
	
	var done = assert.async();
	data.getContactListDFD("10")
		.done(function(results) {
			assert.ok(true, 'Successfully got data');
			assert.deepEqual(results.total, 50, 'There should be a total of 50 records available')
			assert.deepEqual(results.from, 1, 'Should be on the first page');
			assert.deepEqual(results.to, 10, 'Should be 10 contacts per page');
			assert.deepEqual(results.last_page, 5, 'There should be 5 pages');
			assert.ok(results.next_page_url, 'There next page URL should not be null');
			assert.deepEqual(results.per_page, "10", 'There should be 10 contacts per pages');
			assert.notOk(results.prev_page_url, 'There prev page URL should be null');
			assert.ok(results.data, 'An array of contacts should be provided');
			assert.deepEqual(results.data.length, 10, 'Should be 10 contacts in the array');
		})
		.fail(function(error) {
			assert.ok(false, 'Got error: ' + error);
		})
		.always(function() {
			done();	
		});
});

QUnit.test('Test Getting Contact List from Server (limit = "a")', function(assert) {
	var data = ContactData();
	
	var done = assert.async();
	data.getContactListDFD("a")
		.done(function(results) {
			assert.ok(false, 'Should have rejected the promse');
		})
		.fail(function(error) {
			assert.ok(true, 'The promise was properly rejected: ' + error);
		})
		.always(function() {
			done();	
		});
});

QUnit.test('Test Getting Contact List from Server (page = 1.4)', function(assert) {
	var data = ContactData();
	
	var done = assert.async();
	data.getContactListDFD(10, 'last_name', false, 1.4)
		.done(function(results) {
			assert.ok(true, 'The promse was properly accepted');
			assert.deepEqual(results.total, 50, 'There should be a total of 50 records available')
			assert.deepEqual(results.from, 1, 'Should be on the first page');
			assert.deepEqual(results.to, 10, 'Should be 10 contacts per page');
			assert.deepEqual(results.last_page, 5, 'There should be 5 pages');
			assert.ok(results.next_page_url, 'There next page URL should not be null');
			assert.deepEqual(results.per_page, "10", 'There should be 10 contacts per pages');
			assert.notOk(results.prev_page_url, 'There prev page URL should be null');
			assert.ok(results.data, 'An array of contacts should be provided');
			assert.deepEqual(results.data.length, 10, 'Should be 10 contacts in the array');
		})
		.fail(function(error) {
			assert.ok(false, 'The promise was improperly rejected: ' + error);
		})
		.always(function() {
			done();	
		});
});

// QUnit.test('Test Adding a Contact', function(assert) {
// 	var data = ContactData();
// 	
// 	var contact = Contact();
// 	contact.first_name = 'Mike';
// 	contact.last_name = 'Hodges';
// 	
// 	var done = assert.async();
// 	data.saveContactDFD(contact)
// 		.done(function(results) {
// 			assert.deepEqual(results.success, true, 'The addition should succeed');
// 			assert.deepEqual(results.new_contact.first_name, 'Mike', 'The first name should be "Mike"');
// 			assert.deepEqual(results.new_contact.last_name, 'Hodges', 'The last name should be "Hodges"');
// 		})
// 		.fail(function(error) {
// 			assert.ok(false, 'The promise was improperly rejected: ' + error);
// 		})
// 		.always(function() {
// 			done();	
// 		});
// });
// 
// QUnit.test('Test Editing a Contact', function(assert) {
// 	var data = ContactData();
// 	
// 	var contact = Contact();
// 	contact.id = 1365;
// 	contact.first_name = 'Mike';
// 	contact.last_name = 'Hodges';
// 	contact.phone = '864-915-4692';
// 	
// 	var done = assert.async();
// 	data.saveContactDFD(contact)
// 		.done(function(results) {
// 			assert.deepEqual(results.success, true, 'The modification should succeed');
// 			assert.deepEqual(results.updated_contact.first_name, 'Mike', 'The first name should be "Mike"');
// 			assert.deepEqual(results.updated_contact.last_name, 'Hodges', 'The last name should be "Hodges"');
// 			assert.deepEqual(results.updated_contact.phone, '864-915-4692', 'The phone should be "864-915-4692"');
// 		})
// 		.fail(function(error) {
// 			assert.ok(false, 'The promise was improperly rejected: ' + error);
// 		})
// 		.always(function() {
// 			done();	
// 		});
// });
// 
// QUnit.test('Test Deleting a Contact', function(assert) {
// 	var data = ContactData();
// 	
// 	var done = assert.async();
// 	data.deleteContactDFD(1365)
// 		.done(function(results) {
// 			assert.deepEqual(results.success, true, 'The deletion should succeed');
// 		})
// 		.fail(function(error) {
// 			assert.ok(false, 'The promise was improperly rejected: ' + error);
// 		})
// 		.always(function() {
// 			done();	
// 		});
// });
