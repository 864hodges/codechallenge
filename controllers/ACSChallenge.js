///
/// The ASChallenge class is here to ensure things get inialized property per page.
///
/// Auther: Mike Hodges
/// Created: 9/18/2015
///
var ACSChallenge = (function() {
	
	var contactData = ContactData();
	var contactListView = ContactListView();
	var contactDetailView = ContactDetailView();
	var contactAddEditView = ContactAddEditView();
	var contactCtrl = ContactController();
	
	///
	/// Note:
	///    * $('body') services as the event context to ensure everyone sends and listens to the same context.
	///    * The contact controller gets a reference to every view.
	///    * The views get an event context, their display context, and optionally a reference to the controller.
	///
	return {
		///
		/// This method is called when index.html is loaded.
		///
		handleHomePageLoad: function() {
			contactListView.init(contactCtrl, $('body'), $('#listArea'), $('#infoArea'), $('#paging'), $('#sortArea'));
			contactDetailView.init($('body'), $('#selected').find('.profileinfo'));
			contactCtrl.init($('body'), contactData, contactListView, contactDetailView, contactAddEditView);
			
			contactCtrl.getContactList();	
		},
		
		///
		/// This method is called when addEditContact.html is loaded.
		///
		handleAddEditContactPageLoad: function() {
			contactAddEditView.init($('body'), $('#newContact').find('.profileinfo'));
			contactCtrl.init($('body'), contactData, contactListView, contactDetailView, contactAddEditView);
			
			contactCtrl.addEditContact();
		}
		
	}
	
});