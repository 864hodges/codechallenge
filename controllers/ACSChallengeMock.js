var ACSChallengeMock = (function() {
	
	var contactData = ContactDataMock();
	var contactListView = ContactListView();
	var contactDetailView = ContactDetailView();
	var contactAddEditView = ContactAddEditView();
	var contactCtrl = ContactController();
	
	return {
		handleHomePageLoad: function() {
			contactListView.init(contactCtrl, $('body'), $('#listArea'), $('#infoArea'), $('#paging'), $('#sortArea'));
			contactDetailView.init($('body'), $('#selected').find('.profileinfo'));
			contactCtrl.init($('body'), contactData, contactListView, contactDetailView, contactAddEditView);
			
			contactCtrl.getContactList();	
		},
		handleAddEditContactPageLoad: function() {
			contactAddEditView.init($('body'), $('#newContact').find('.profileinfo'));
			contactCtrl.init($('body'), contactData, contactListView, contactDetailView, contactAddEditView);
			
			contactCtrl.addEditContact();
		}
		
	}
	
});