///
/// The ContactController class is here to manage communication between data and view components.
///
/// Auther: Mike Hodges
/// Created: 9/18/2015
///
var ContactController = function() {
	
	///
	/// This section contains the business logic.
	///
	var Business = {
		///
		/// This method gets the contact list from the service and then hands it off via event to
		/// any interested view.
		///
		/// Note:
		///    * A call is made to the data component to get the list. A jQuery deferred object
		///      is returned.
		///
		getContactList: function(self, limit, sort, desc, page) {
			self.listView.showWaiting();
			
			// Get the list of contacts
			self.contactData.getContactListDFD(limit, sort, desc, page)
				.done(function(results) {
					// Automatically select the first contact to show the details
					Business.contactSelectedFromList(self, results.data[0].id);	

					// Send the results to any interested party
					var event = $.Event('contactListReceived', {results:results});
					self.eventContext.trigger(event);				
				})
				.fail(function(error) {
					self.listView.showError(error);
				});
		},
		
		///
		/// This method gets the details for a specific contact by id.The contact
		/// is then send via 'selected for display' event.
		///
		contactSelectedFromList: function(self, contactID) {
			self.detailView.showWaiting();
			
			// Get the contact details
			self.contactData.getContactByIDDFD(contactID)
				.done(function(result) {
					// Send the results to any interested parth
					var event = $.Event('contactSelectedForDisplay', result);
					self.eventContext.trigger(event);	
				})
				.fail(function(error) {
					self.listView.showError(error);
				});
		},
		
		///
		/// This method gets the details for a specific contact by id.  The contact
		/// is then send via 'selected for edit' event.
		///
		contactSelectedForEdit: function(self, contactID) {
			self.addEditView.showWaiting();
			
			// Get the contact details
			self.contactData.getContactByIDDFD(contactID)
				.done(function(result) {
					// Send the results to any interested parth
					var event = $.Event('contactSelectedForEdit', result);
					self.eventContext.trigger(event);	
				})
				.fail(function(error) {
					self.listView.showError(error);
				});
		},
		
		///
		/// This method deletes a specific contact by id.  The list is then refreshed.
		///
		deleteContact: function(self, contactID) {
			if (confirm('Are you sure you want to delete the contact?')) {
				
				// Delete the contact
				self.contactData.deleteContactDFD(contactID)
					.done(function(result) {
						if (result.success) {
							self.detailView.showDeleted('The contact has been deleted.');
							self.detailView.hideDeleted(function() {
								// Refresh the contact list and the displayed contact, after the message goes away.
								Business.getContactList(self, self.currentLimit, self.currentSort, self.currentDesc, self.currentPage);
							});							
						} else {
							self.listView.showError('Something went wrong deleting the contact.');
						}
					})
					.fail(function(error) {
						self.listView.showError(error);
					});
			}
		},
		
		///
		/// This method adds a new contact or modifies an existing contact.
		///
		saveContact: function(self, contact) {
			var isEdit = false;
			if (contact.id) { isEdit = true; }
			
			self.addEditView.showSaving();
			
			// Save the contact
			self.contactData.saveContactDFD(contact)
				.done(function(result) {
					if (result.success) {
						// Show the appropriate message and then refresh the page if adding a contact.
						if (isEdit) {
							self.addEditView.showSaved(result.updated_contact.first_name + ' ' + result.updated_contact.last_name + ' was successfully updated!');
							self.addEditView.hideSaved();
						} else {
							self.addEditView.showSaved(result.new_contact.first_name + ' ' + result.new_contact.last_name + ' was successfully added!');
							self.addEditView.hideSaved(function() {
								window.location = "index.html";
							});							
						}
					} else {
						self.listView.showError(result.errors.first_name[0]);
					}
				})
				.fail(function(error) {
					self.listView.showError(error);
				})
				.always(function() {
					self.addEditView.hideSaving();
				});
		}
	};
	
	///
	/// This section contains the logic to save to and retrieve from local storage.
	///
	var Storage = {
		///
		/// This method loads the data from local storage for use by the application.
		///
		load: function(self) {
			self.currentLimit = parseInt(localStorage.getItem('limit'));
			self.currentSort = localStorage.getItem('sort') === 'null' ? null : localStorage.getItem('sort');
			self.currentDesc = localStorage.getItem('desc') === 'true' ? true : false;
			self.currentPage = parseInt(localStorage.getItem('page'));
		},
		
		///
		/// This method save data to local storage to maintain the user's selections.
		///
		save: function(self) {
			localStorage.setItem('limit', self.currentLimit);
			localStorage.setItem('sort', self.currentSort);
			localStorage.setItem('desc', self.currentDesc);
			localStorage.setItem('page', self.currentPage);
		},
		
		///
		/// This method gets a specific setting from storage.
		///
		getSetting: function(settingName) {
			return localStorage.getItem(settingName);
		},
		
		///
		/// This method saves a specific setting in storage.
		///
		saveSetting: function(settingName, settingValue) {
			localStorage.setItem(settingName, settingValue);	
		},
		
		///
		/// This method clears the local storage.
		///
		clear: function() {
			localStorage.clear();
		}
		
	};
	
	return {
		
		// A reference to the data component, event context, and views
		eventContext: null,
		contactData: null,
		listView: null,
		detailView: null,
		addEditView: null,
		
		// The list settings for this session
		currentLimit: null,
		currentSort: null,
		currentDesc: null,
		currentPage: null,
		
		///
		/// This method initializes the ContactContoller instance by accepting refences to its dependencies
		/// and listening for pertinent events.
		///
		init: function(eventContext, data, contactListView, contactDetailView, contactAddEditView) {
			var self = this;
			self.eventContext = eventContext;
			self.contactData = data;
			self.listView = contactListView;
			self.detailView = contactDetailView;
			self.addEditView = contactAddEditView;
			
			// Load the settings from last session
			Storage.load(self);
			
			// Listen for list-related events
			self.eventContext.bind('contactSelectedFromList', function(event) {
				Business.contactSelectedFromList(self, event.id);
			});
			self.eventContext.bind('listSizeChanged', function(event) {
				self.currentLimit = event.size;
				self.currentPage = 1;
				Storage.save(self);
				
				Business.getContactList(self, self.currentLimit, self.currentSort, self.currentDesc, self.currentPage);
			});
			self.eventContext.bind('nextPageClicked', function() {
				++self.currentPage;				
				Storage.save(self);
				
				Business.getContactList(self, self.currentLimit, self.currentSort, self.currentDesc, self.currentPage);
			});
			self.eventContext.bind('prevPageClicked', function() {
				--self.currentPage;				
				Storage.save(self);
				
				Business.getContactList(self, self.currentLimit, self.currentSort, self.currentDesc, self.currentPage);
			});
			self.eventContext.bind('sortButtonClicked', function(event) {
				if (event.sort === 'date') {
					self.currentSort = 'created_at';
					self.currentDesc = true;					
				} else if (event.sort === 'lastName') {
					self.currentSort = 'last_name';
					self.currentDesc = false;										
				} else if (event.sort === 'firstName') {
					self.currentSort = 'first_name';
					self.currentDesc = false;										
				} else {
					self.currentSort = 'created_at';
					self.currentDesc = true;					
				}				
				self.currentPage = 1;
				Storage.save(self);
				
				Business.getContactList(self, self.currentLimit, self.currentSort, self.currentDesc, self.currentPage);
			});
			
			// Listen for manipulation events
			self.eventContext.bind('addContactClicked', function() {
				window.location = "addEditContact.html";
			});
			self.eventContext.bind('editContactClicked', function(event) {
				window.location = "addEditContact.html?id=" + event.id;
			});
			self.eventContext.bind('deleteContactClicked', function(event) {
				Business.deleteContact(self, event.id);
			});
			self.eventContext.bind('saveContactClicked', function(event) {
				Business.saveContact(self, event.contact);
			});
			self.eventContext.bind('cancelSaveContactClicked', function() {
				window.location = "index.html";
			});
			
		},
		
		///
		/// This method gets the contact list from the service and then hands it off via event to
		/// any interested view.
		///
		/// Note:
		///    * A call is made to the data component to get the list. A jQuery deferred object
		///      is returned.
		///
		getContactList: function(limit, sort, desc, page) {
			this.currentLimit = limit || this.currentLimit || 10;
			this.currentSort = sort || this.currentSort;
			this.currentDesc = desc || this.currentDesc || false;
			this.currentPage = page || this.currentPage || 1;
			Storage.save(this);

			// Do the real work in the business component			
			Business.getContactList(this, this.currentLimit, this.currentSort, this.currentDesc, this.currentPage);
		},
		
		///
		/// This method gets the details for a specific contact by id.The contact
		/// is then send via 'selected for display' event.
		///
		contactSelectedFromList: function(contactID) {
			// Do the real work in the business component			
			Business.contactSelectedFromList(this, contactID);	
		},
		
		///
		/// This method gets the details for a specific contact by id.  The contact
		/// is then send via 'selected for edit' event.
		///
		contactSelectedForEdit: function(contactID) {
			// Do the real work in the business component			
			Business.contactSelectedForEdit(this, contactID);
		},
		
		///
		/// This makes the contactSelectedForEdit method publicly accessible. More importantly, this
		/// method is called by the ACSChallenge controller when the addEditContact.html page is loaded.
		/// This method is responsible for getting the contact ID from the query string, if present.
		///
		addEditContact: function() {
			var url = window.location.search;
			if (url.indexOf('?') === -1) {
				// add 
				return; 
			}
			
			var counter = 0;
			var parameters = url.split('?')[1].split('&');
			for (counter; counter<parameters.length; counter++) {
				var param = parameters[counter].split('=');
				if (param[0].toUpperCase() === 'ID') {
					// Do the real work in the business component			
					Business.contactSelectedForEdit(this, parseInt(param[1]));
					return;
				}
			}
		},
		
		///
		/// This makes the getSetting method publicly accessible.
		///
		getSetting: function(settingName) {
			return Storage.getSetting(settingName);
		},
		///
		/// This makes the saveSetting method publicly accessible.
		///
		saveSetting: function(settingName, settingValue) {
			Storage.saveSetting(settingName, settingValue);
		},
		///
		/// This makes the clear storage method publicly accessible.
		///
		clearSettings: function() {
			Storage.clear();
		}
		
	};
	
};