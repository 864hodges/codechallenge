///
/// The Contact class is here to model the contact on the server.
///
/// Auther: Mike Hodges
/// Created: 9/18/2015
///
var Contact = function() {	
	return {
		id: null,
		first_name: null,
		last_name: null,
		company_name: null,
		address: null,
		city: null,
		state: null,
		zip: null,
		phone: null,
		work_phone: null,
		email: null,
		url: null,
		created_at: null,
		updated_at: null
	}
};